#include <iostream>
#include <string>
#include <fstream>
#include "ArquivoImagem.hpp"

using namespace std;

ArquivoImagem::ArquivoImagem(){
	 setNome("lena.pgm");
	 setAltura(0);
	 setLargura(0);
	 setNumMagico("P5");
	 setBit_extraido('A');
	
}

ArquivoImagem::ArquivoImagem(string nome, int altura, int largura, std::string NumMagico, char bit_extraido){
	void setNome(std::string nome);
	void setAltura(int altura);
	void setLargura(int largura);
	void setNumMagico(std::string NumMagico);
	void setBit_extraido(char bit_extraido);
}

ArquivoImagem::~ArquivoImagem(){

}

std::string ArquivoImagem::getNome(){
	return nome;
}

void ArquivoImagem::setNome(string nome){
	this->nome=nome;
}

void ArquivoImagem::setAltura (int altura){
	this->altura=altura;
}

int ArquivoImagem::getAltura(){
	return altura;
}
void ArquivoImagem::setLargura (int largura){
	this->largura=largura;
}

int ArquivoImagem::getLargura(){
	return largura;
}

void ArquivoImagem::setNumMagico (std::string NumMagico){
	this->NumMagico=NumMagico;
}

string ArquivoImagem::getNumMagico(){
	return NumMagico;
}

void ArquivoImagem::setBit_extraido(char bit_extraido){
	this->bit_extraido=bit_extraido;
}

char ArquivoImagem::getBit_extraido(){
	return bit_extraido;
}


void ArquivoImagem::AbrirImagem(void){
	
	std::string nome_arquivo;
		
	cout << "Escreva o nome ou o diret�rio do arquivo que deseja abrir" << endl;
	cin >> nome_arquivo;

	fstream file (nome_arquivo.c_str());

	 if (file==NULL) {
	        cout << "Arquivo nao encontrado"<<endl;
	}else{
			cout << "Arquivo encontrado e ser� aberto em instantes"<<endl;
	}
	
}



