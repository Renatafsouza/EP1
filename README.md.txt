-> O programa realiza a abertura de um arquivo.

-> Para compilar:
	Abra o terminal, acesse a pasta EP_1. Digite "make", 
na linha de comando. A compila��o � iniciada.

-> O programa d� diretrizes para acess�-lo. Primeiramente pede
para que o nome de um arquivo ou seu diret�rio seja inserido.
Basta digitar, tudo na mesma linha, e de prefer�ncia sem espa�o.
Ser� emitida uma mensagem de erro caso o arquivo n�o seja encontrado.
Observa��o: Para arquivos dentro da pasta, basta que se insira o nome.
Para arquivos fora da pasta do programa, � necess�rio que todo o 
diret�rio do arquivo seja inserido.