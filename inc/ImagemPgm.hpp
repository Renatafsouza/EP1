#ifndef IMAGEMPGM_H
#define IMAGEMPGM_H
#include <string>
#include "ArquivoImagem.hpp"

class ImagemPgm : public ArquivoImagem {
private:
	int posicao_inicial;

public:
    ImagemPgm();
	ImagemPgm(std::string nome, int altura, int largura, std::string NumMagico, int posicao_inicial, char bit_extraido);  
	~ImagemPgm();  
	void setPosicao_inicial(int posicao_inicial);
	int getPosicao_inicial();    


	void DecifraMensagem(std::string NumMagico);
	
};

#endif

