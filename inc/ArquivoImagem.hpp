#ifndef ARQUIVOIMAGEM_H
#define ARQUIVOIMAGEM_H

#include <iostream>
#include <string>

class ArquivoImagem {
private:
        std::string nome;
        int altura;
        int largura;
        std::string NumMagico;
        char bit_extraido;
public:
        ArquivoImagem();
        ArquivoImagem(std::string nome, int altura, int largura, std::string NumMagico, char bit_extraido);
        ~ArquivoImagem();
        void setNome(std::string nome);
        std::string getNome();
        void setAltura(int altura);
        int getAltura();
        void setLargura(int largura);
        int getLargura();
        void setNumMagico(std::string NumMagico);
        std::string getNumMagico();
        void setBit_extraido(char bit_extraido);
        char getBit_extraido();
		void AbrirImagem(void);

        

};

#endif

