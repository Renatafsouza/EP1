#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP
#include "ArquivoImagem.hpp"

class ImagemPpm : public ArquivoImagem{
	public:
		
		ImagemPpm();
		ImagemPpm(std::string nome, int altura, int largura, std::string NumMagico, char bit_extraido);
		~ImagemPpm();
		void AplicarFiltro();	
	
};
#endif

